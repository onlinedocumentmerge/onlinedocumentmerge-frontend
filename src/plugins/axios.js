import axios from "axios";

const service = axios.create({
	//baseURL: "http://10.10.10.153:8080/",
	baseURL: "http://localhost:80/",
	headers: {
		"Access-Control-Allow-Origin": "*",
		"Access-Control-Allow-Methods": "*",
		"Content-Type": "application/json"
	}
});
export default service;
// export default {
// 	install(Vue) {
// 		Vue.prototype.$axios = axios.create({
// 			//baseURL: "https://inox.azurewebsites.net/",
// 			//baseURL: "http://172.16.2.179:8080/",
// 			baseURL: "http://localhost:8080/",
// 			headers: {
// 				"Access-Control-Allow-Origin": "*",
// 				"Access-Control-Allow-Methods": "*",
// 				"Content-Type": "application/json"
// 			}
// 		});
// 	}
// };
