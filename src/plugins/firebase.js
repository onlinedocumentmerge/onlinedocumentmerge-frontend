import Vue from "vue";
import { firestorePlugin } from "vuefire";
import firebase from "firebase/app";
import "firebase/firestore";
import "firebase/messaging";
import "firebase/storage";
import "firebase/auth";

Vue.use(firestorePlugin);

const firebaseConfig = {
	apiKey: "AIzaSyCEeuQImU9yTl3TsQhh8YuqDgoq7VP_ON0",
	authDomain: "onlinedocumentmerge.firebaseapp.com",
	databaseURL: "https://onlinedocumentmerge.firebaseio.com",
	projectId: "onlinedocumentmerge",
	storageBucket: "onlinedocumentmerge.appspot.com",
	messagingSenderId: "94472260922",
	appId: "1:94472260922:web:05375b7aa7037f30f85403",
	measurementId: "G-PYL5B69MZW"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

// Vue.prototype.$messaging = firebase.messaging()
// Vue.prototype.$messaging.usePublicVapidKey(
// 			"BNrewsZUKDQHzF5NRez_5PGeUPd-6BpuQz9zl0ywnE9482bgCRi8Qiv0O57i6TxsbvhprH9gJURNsOU7yKxnevs"
// 		);

// navigator.serviceWorker
// 	.register("firebase-messaging-sw.js")
// 	.then(registration => {
// 		Vue.prototype.$messaging.useServiceWorker(registration);
// 	})
// 	.catch(err => {
// 		console.log(err);
// 	});
// 	navigator.serviceWorker.addEventListener("message", (message) => console.log("plapla", message));
export const db = firebase.firestore();
export const storage = firebase.storage();
export const auth = firebase.auth();
// export const message = firebase
// 	.messaging()
// 	.usePublicVapidKey(
// 		"BNrewsZUKDQHzF5NRez_5PGeUPd-6BpuQz9zl0ywnE9482bgCRi8Qiv0O57i6TxsbvhprH9gJURNsOU7yKxnevs"
// 	);
