import Vue from 'vue'
import Vuex from 'vuex'

import account from '@/store/modules/account.js'
import editor from '@/store/modules/editor.js'
import datasource from '@/store/modules/datasource.js'
import job from '@/store/modules/job.js'
import template from '@/store/modules/template.js'
import arithmetic from '@/store/modules/arithmetic.js'
import smtp from '@/store/modules/smtp.js'

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        account,
        editor,
        datasource,
        job,
        template,
        arithmetic,
        smtp
    }
})
