import { getAll, get, deleteByID, connectDBMS, getTableDBMSData, create, update } from "@/service/datasource";

const state = {
	headers: [],
	rows: [],
	edittingRow: [],
	edittingIndex: -1,
	isEdittingRow: false,
	originalRow: {},
	datasource: {},
	datasources: [],
	deletedRows: []
};

// getters
const getters = {
	datasources(state) {
		return state.datasources;
	},
	datasource(state) {
		return state.datasource;
	},
	headers(state) {
		let headersLength = state.headers.length;
		if (headersLength > 0) {
			let lastItem = state.headers[headersLength - 1];
			if (!lastItem.value === "action") {
				let actionHeader = {
					text: "Actions",
					value: "action",
					sortable: false
				};
				state.headers.push(actionHeader);
			}
		}
		return state.headers;
	},
	rows(state) {
		return state.rows;
	},
	edittingRow(state) {
		return state.edittingRow;
	},
	isEdittingRow(state) {
		return state.isEdittingRow;
	},
	originalRow(state) {
		return state.originalRow;
	},
	edittingIndex(state) {
		return state.edittingIndex;
	},
	deletedRows(state) {
		return state.deletedRows;
	}
};
// mutations
const mutations = {
	updateDatasources(state, datasources) {
		state.datasources = datasources;
	},
	updateDatasource(state, datasource) {
		state.datasource = datasource;
	},
	addHeader(state, header) {
		let headersLength = state.headers.length;
		if (headersLength == 0) {
			let actionHeader = {
				text: "Action",
				value: "action",
				sortable: false,
				datatypes: []
			};
			state.headers.push(actionHeader);
		}
		state.headers.splice(headersLength - 1, 0, header);
	},
	updateHeader(state, header, index) {
		state.headers[index] = header;
	},
	removeHeader(state, header) {
		let index = state.headers.indexOf(header);
		state.headers.splice(index, 1);
		if(state.headers.length == 1) {
			state.headers = [];
		}
	},
	addRow(state, row) {
		state.rows.push(row);
	},
	editRow(state, payload) {
		state.edittingIndex = payload.edittingIndex;
		state.edittingRow = payload.edittingRow;
		state.originalRow = payload.row;
		state.isEdittingRow = true;
	},
	updateRow(state, payload) {
		if((state.datasource.id == null && Object.keys(payload.row).length == 1) || (state.datasource.id != null && Object.keys(payload.row).length == 2)) {
			state.rows.splice(payload.index, 1);
		} else {
		state.rows.splice(payload.index, 1, payload.row);
		}
		state.edittingIndex = -1;
		state.originalRow = {};
		state.isEdittingRow = false;
	},
	removeRow(state, row) {
		let index = state.rows.indexOf(row);
		state.rows.splice(index, 1);
		if(state.datasource != null) {
			state.deletedRows.push(row.ODM)
		}
	},
	resetData(state) {
		state.headers = [];
		state.rows = [];
	},
	addEdittingRow(state, row) {
		state.edittingRow.push(row);
	},
	updateEdittingRow(state, payload) {
		state.edittingRow.splice(payload.index, 1, payload.row);
	},
	deleteEdittingRow(state, index) {
		state.rows.splice(index, 1);
	},
	resetEdittingRow(state) {
		state.edittingRow = [];
	},
	resetValueEdittingRow(state, rows) {
		state.edittingRow = rows;
	},
	updateIsEdittingRow(state, status) {
		state.isEdittingRow = status;
	},
	clearDeletedRows(state) {
		state.deletedRows = [];
	}
};
// actions
const actions = {
	async getDatasources({commit}) {
		const result = await getAll();
		if(result.status === 200) {
			commit('updateDatasources', result.data);
		}
		return result;
	},
	async getDatasource({commit}, id) {
		const result = await get(id);
		if(result.status === 200) {
			commit('updateDatasource', result.data);
		}
		return result;
	},
	async deleteDatasource({dispatch}, id) {
		const result = await deleteByID(id);
		if(result.status === 200) {
			await dispatch('getDatasources');
		}
		return result;
	},
	//one payload
	async connectDBMS({commit}, payload) {
		const result = await connectDBMS(payload.connectionString, payload.typeConnection);
		return result;
	},
	async getDataFromTable({commit}, payload) {
		const result = await getTableDBMSData(payload.connectionString, payload.typeConnection, payload.tableName, payload.limit, payload.pageNumber, payload.statement);
		return result;
	},
	async create({commit}, datasource) {
		const result = await create(datasource);
		if(result.status === 200) {
			let data = result.data;
			commit('updateDatasource', data);
		}
		return result;
	},

	async update({commit}, datasource) {
		const result = await update(datasource);
		if(result.status === 200) {
			commit('updateDatasource', datasource);
		}
		return result;
	},
	updateDatasource({commit}, datasource) {
		commit('updateDatasource', datasource);
	},
	addHeader({commit}, header) {
		commit("addHeader", header);
	},
	updateHeader({commit}, header, index) {
		commit("updateHeader", header, index);
	},
	removeHeader({commit}, header) {
		commit("removeHeader", header);
	},
	addRow({commit}, row) {
		commit("addRow", row);
	},
	editRow({commit}, payload) {
		commit("editRow", payload);
	},
	updateRow({commit}, payload) {
		commit("updateRow", payload);
	},
	removeRow({commit}, row) {
		commit("removeRow", row);
	},
	resetData({commit}) {
		commit("resetData");
	},
	addEdittingRow({commit}, row) {
		commit("addEdittingRow", row);
	},
	updateEdittingRow({commit}, payload) {
		commit("updateEdittingRow", payload);
	},
	deleteEdittingRow({commit}, index) {
		commit("deleteEdittingRow", index);
	},
	resetEdittingRow({commit}) {
		commit("resetEdittingRow");
	},
	resetValueEdittingRow({commit}, rows) {
		commit("resetValueEdittingRow", rows);
	},
	updateIsEdittingRow({commit}, status) {
		commit("updateIsEdittingRow", status);
	},
	clearDeletedRows({commit}) {
		commit("clearDeletedRows");
	}
};
export default {
	namespaced: true,
	state,
	getters,
	actions,
	mutations
};
