import {
	getAll,
	get,
	create,
	execute,
	deleteJob,
	pause,
	resume
} from "@/service/job";
const state = {
	jobs: [
		{
			id: 4,
			// name: "Job Test",
			// sendingEmail: true,
			// downloadPDF: true,
			// specificLink: true,
			// price: 2.6,
			// specificDomain: "odm.com",
			// rootParameter: "transcipt",
			// completed: 5,
			// amount: 10,
			// failed: 5,
			// status: 5,
			// pdfStorageURL: ""
		}
	],
	job: {
		name: "Job " + new Date().toISOString().substr(0, 10)
	}
};

const getters = {
	jobs(state) {
		return state.jobs;
	},
	job(state) {
		return state.job;
	}
};

const mutations = {
	updateJobs(state, jobs) {
		state.jobs = jobs;
	},
	updateJob(state, job) {
		state.job = job;
	}
};

const actions = {
	async getJobs({ commit }) {
		const result = await getAll();
		if (result.status === 200) {
			commit("updateJobs", result.data);
		}
		return result;
	},
	async getJob({ commit }, id) {
		const result = await get(id);
		if (result.status === 200) {
			commit("updateJob", result.data);
		}
		return result;
	},
	async createJob({ dispatch }, job) {
		const result = await create(job);
		if (result.status === 200) {
			await dispatch("getJobs");
		}
		return result;
	},
	async deleteJob({ dispatch }, job) {
		const result = await deleteJob(job);
		if (result.status === 200) {
			await dispatch("getJobs");
		}
		return result;
	},
	async executeJob({ dispatch }, id) {
		console.log("ID  " + id);
		const result = await execute(id);
		return result;
	},
	async pauseJob({ dispatch }, id) {
		console.log("ID  " + id);
		const result = await pause(id);
		return result;
	},
	async resumeJob({ dispatch }, id) {
		console.log("ID  " + id);
		const result = await resume(id);
		return result;
	},
	updateJob({ commit }, job) {
		commit("updateJob", job);
	}
};

export default {
	namespaced: true,
	state,
	getters,
	actions,
	mutations
};
