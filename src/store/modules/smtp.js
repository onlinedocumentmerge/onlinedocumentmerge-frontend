import { getAll, get, create, update, deleteSMTP, sendTestEmail } from "@/service/smtp"

const state = {
    smtps: [],
    smtp: {}
}

const getters = {
    smtps(state) {
        return state.smtps;
    },
    smtp(state) {
        return state.smtp;
    }
}

const mutations = {
    updateSMTPS(state, smtps) {
        state.smtps = smtps;
    },
    updateSMTP(state, smtp) {
        state.smtp = smtp;
    }
}

const actions = {
    async sendTestEmail({commit}, smtp) {
        const result = await sendTestEmail(smtp);
        return result;
    },

    async getAll({commit}) {
        const result = await getAll();
        if(result.status === 200) {
            commit('updateSMTPS', result.data);
        }
        return result;
    },

    async create({dispatch}, smtp) {
        const result = await create(smtp);
        if(result.status === 200) {
            dispatch('getAll');
        }
        return result;
    },

    async update({dispatch}, smtp) {
        const result = await update(smtp);
        if(result.status === 200) {
            dispatch('getAll');
        }
        return result;
    },

    async delete({dispatch}, id) {
        const result = await deleteSMTP(id);
        if(result.status === 200) {
            dispatch('getAll');
        }
        return result;
    }
}

export default {
	namespaced: true,
	state,
	getters,
	actions,
	mutations
}