import {getAll, get, create, deleteTemplate} from "@/service/template"
const state = {
    templates: [],
    template: {
        name: "Template " + new Date().toISOString().substr(0, 10)
    }
}

const getters = {
    templates(state) {
        return state.templates;
    },
    template(state) {
        return state.template;
    }
}

const mutations = {
    updateTemplates(state, templates) {
        state.templates = templates;
    },
    updateTemplate(state, template) {
        state.template = template;
    }
}

const actions = {
    async getTemplates({commit}) {
        const result = await getAll();
        if(result.status === 200) {
            commit('updateTemplates', result.data);
        }
        return result;
    },
    async getTemplate({commit}, id) {
        const result = await get(id);
        if(result.status === 200) {
            commit('updateTemplate', result.data);
        }
        return result;
    },
    async createTemplate({commit, dispatch}, template) {
        const result = await create(template);
        if(result.status === 200) {
            commit('updateTemplate', result.data)
            dispatch('getTemplates');
        }
        return result;
    },
    async deleteTemplate({dispatch}, id) {
        const result = await deleteTemplate(id);
        if(result.status === 200) {
            dispatch('getTemplates');
        }
        return result;
    },
    updateTemplate({commit}, template) {
        commit('updateTemplate', template);
    }
}

export default {
	namespaced: true,
	state,
	getters,
	actions,
	mutations
}