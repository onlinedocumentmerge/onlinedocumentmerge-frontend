import { create, update, deleteArithmetic, getAll } from "@/service/arithmetic"

const state = {
    // arithmetics: [{
    //     operator: "SUM",
    //     minParam: 2,
    //     maxParam: 2147483647,
    //     description: "Sum Function"
    // }, {
    //     operator: "AVG",
    //     minParam: 1,
    //     maxParam: 2147483647,
    //     description: "Hàm Tính Trung Bỉnh"
    // }, {
    //     operator: "SALARY",
    //     minParam: 4,
    //     maxParam: 4,
    //     description: "Hàm Tính Lương"
    // }]
    arithmetics: []
}

const getters = {
    arithmetics(state) {
        return state.arithmetics;
    }
}

const mutations = {
    updateArithmetics(state, arithmetics) {
        state.arithmetics = arithmetics;
    }
}

const actions = {
    async getAll({commit}) {
        const result = await getAll();
        if(result.status === 200) {
            console.log(result.data);
            commit('updateArithmetics', result.data);
        }
        return result;
    },

    async create({dispatch}, arithmetic) {
        const result = await create(arithmetic);
        if(result.status === 200) {
            //dispatch(getAll);
        }
        return result;
    },

    async update({dispatch}, arithmetic) {
        const result = await update(arithmetic);
        if(result.status === 200) {
            dispatch(getAll);
        }
        return result;
    },

    async delete({dispatch}, arithmeticID) {
        const result = await deleteArithmetic(arithmeticID);
        if(result.status === 200) {
            dispatch(getAll);
        }
        return result;
    },
}

export default {
	namespaced: true,
	state,
	getters,
	actions,
	mutations
}