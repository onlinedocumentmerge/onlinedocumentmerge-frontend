import { login, register, getProfile } from "@/service/account"
const state = {
	account:
		localStorage.getItem("account") != undefined
			? JSON.parse(localStorage.getItem("account"))
			: null
};

const getters = {
	account(state) {
		return state.account;
	}
};

const mutations = {
	updateAccount(state, account) {
		state.account = account;
		localStorage.setItem('account', JSON.stringify(account));
		localStorage.setItem('token', account.token);
	},
	logout(state) {
		state.account = null;
		localStorage.removeItem('account');
	}
};

const actions = {
	async login({ commit }, account) {
		const result = await login(account);
		if(result.status === 200) {
			commit("updateAccount", result.data);
		}
		//return result.status === 200;
		return result;
	},
	async getProfile({ commit }) {
		const result = await getProfile();
		if(result.status === 200) {
			// commit("updateAccount", result.data);
		}
		return result;
	},
	async register({ commit }, account) {
		const result = await register(account);
		return result;
	},
	updateAccount({ commit }, account) {
		commit("updateAccount", account);
	},
	logout({ commit }) {
		commit("logout");
	}
};

export default {
	namespaced: true,
	state,
	getters,
	actions,
	mutations
};
