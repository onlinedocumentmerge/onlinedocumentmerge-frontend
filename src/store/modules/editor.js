// initial state
const state = {
    documents: [],
	insertedHeaders: [],
	content: "<p></p>"
}

// getters
const getters = {
	content(state) {
		if (state.content == "") {
			state.content = "<p></p>";
		}
		return state.content;
	}
}

// mutations
const mutations = {
	setContent(state, newContent) {
		state.content = newContent;
	}
}

// actions
const actions = {
	setContent(context, newContent) {
		context.commit("setContent", newContent);
	}
}

export default {
	namespaced: true,
	state,
	getters,
	actions,
	mutations
}