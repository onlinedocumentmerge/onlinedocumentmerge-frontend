import axios from "@/plugins/axios";

const URL = "api/datasources";

export function getAll() {
	return axios.get(`${URL}`, {
		headers: {
			Authorization: "Bearer " + localStorage.getItem("token")
		}
	});
}

export function get(id) {
	return axios.get(`${URL}/${id}`, {
		headers: {
			Authorization: "Bearer " + localStorage.getItem("token")
		}
	});
}

export function deleteByID(id) {
	return axios.delete(`${URL}/${id}`, {
		headers: {
			Authorization: "Bearer " + localStorage.getItem("token")
		}
	});
}

export function connectDBMS(connectionString, typeConnect) {
	return axios.get(`${URL}/dbms`, {
		headers: {
			Authorization: "Bearer " + localStorage.getItem("token")
		},
		params: {
			connectionString: connectionString,
			typeConnect: typeConnect
		}
	});
}

export function getTableDBMSData(connectionString, typeConnect, tableName, limit, pageNumber, statement) {
	return axios.get(`${URL}/dbms/table`, {
		headers: {
			Authorization: "Bearer " + localStorage.getItem("token")
		},
		params: {
			connectionString: connectionString,
			typeConnect: typeConnect,
			tableName: tableName,
			limit: limit,
			pageNumber: pageNumber,
			statement: statement
		}
	});
}

export function create(datasource) {
	return axios.post(`${URL}`, datasource, {
		headers: {
			Authorization: "Bearer " + localStorage.getItem("token")
		}
	});
}

export function update(datasource) {
	return axios.put(`${URL}`, datasource, {
		headers: {
			Authorization: "Bearer " + localStorage.getItem("token")
		}
	});
}
