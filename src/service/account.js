import axios from '@/plugins/axios'

const URL = 'api/accounts';

export function login(account) {
    return axios.post(`${URL}/login`, account)
}

export function register(account) {
    return axios.post(`${URL}/register`, account)
}

export function getProfile() {
    return axios.get(`${URL}/profile`, {
        headers: {
            Authorization: "Bearer " + localStorage.getItem("token")
        }
    })
}