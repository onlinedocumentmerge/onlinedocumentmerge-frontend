import axios from "@/plugins/axios"
const URL = "api/templates"

export function getAll() {
    return axios.get(`${URL}`, {
        headers: {
            Authorization: "Bearer " + localStorage.getItem("token")
        }
    });
}

export function get(id) {
    return axios.get(`${URL}/${id}`, {
        headers: {
            Authorization: "Bearer " + localStorage.getItem("token")
        }
    });
}

export function deleteTemplate(id) {
    return axios.delete(`${URL}/${id}`, {
        headers: {
            Authorization: "Bearer " + localStorage.getItem("token")
        }
    });
}

export function create(template) {
    return axios.post(`${URL}`, template, {
        headers: {
            Authorization: "Bearer " + localStorage.getItem("token")
        }
    });
}