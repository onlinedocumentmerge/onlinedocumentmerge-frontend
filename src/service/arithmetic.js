import axios from "@/plugins/axios"

const URL = "api/arithmetics"

export function create(arithmetic) {
    return axios.post(`${URL}`, arithmetic, {
        headers: {
            Authorization: "Bearer " + localStorage.getItem("token"),
            'Content-Type': 'multipart/form-data'
        }
    });
}

export function update(arithmetic) {
    return axios.put(`${URL}`, arithmetic, {
        headers: {
            Authorization: "Bearer " + localStorage.getItem("token")
        }
    });
}

export function deleteArithmetic(arithmeticID) {
    return axios.delete(`${URL}`, arithmeticID, {
        headers: {
            Authorization: "Bearer " + localStorage.getItem("token")
        }
    });
}

export function getAll() {
    return axios.get(`${URL}`, {
        headers: {
            Authorization: "Bearer " + localStorage.getItem("token")
        }
    });
}