import axios from "@/plugins/axios"

const URL = "api/jobs";

export function getAll() {
    return axios.get(`${URL}`, {
        headers: {
            Authorization: "Bearer " + localStorage.getItem("token")
        }
    });
}

export function get(id) {
    return axios.get(`${URL}/${id}`, {
        headers: {
            Authorization: "Bearer " + localStorage.getItem("token")
        }
    });
}

export function deleteJob(id) {
    return axios.delete(`${URL}/${id}`, {
        headers: {
            Authorization: "Bearer " + localStorage.getItem("token")
        }
    });
}

export function create(job) {
    return axios.post(`${URL}`, job, {
        headers: {
            Authorization: "Bearer " + localStorage.getItem("token")
        }
    });
}

export function execute(id) {
    console.log("ID: " + id);
    return axios.post(`${URL}/${id}/execute`, {
        headers: {
            Authorization: "Bearer " + localStorage.getItem("token")
        }
    });
}
export function pause(id) {
    console.log("ID: " + id);
    return axios.post(`${URL}/${id}/pause`, {
        headers: {
            Authorization: "Bearer " + localStorage.getItem("token")
        }
    });
}
export function resume(id) {
    console.log("ID: " + id);
    return axios.post(`${URL}/${id}/resume`, {
        headers: {
            Authorization: "Bearer " + localStorage.getItem("token")
        }
    });
}