import axios from "@/plugins/axios"

const URL = "api/smtps";

export function sendTestEmail(smtp) {
    return axios.post(`${URL}/sendTestEmail`, smtp);
}

export function getAll() {
    return axios.get(`${URL}`, {
        headers: {
            Authorization: "Bearer " + localStorage.getItem("token")
        }
    });
}

export function get(id) {
    return axios.get(`${URL}/${id}`, {
        headers: {
            Authorization: "Bearer " + localStorage.getItem("token")
        }
    });
}

export function create(smtp) {
    return axios.post(`${URL}`, smtp, {
        headers: {
            Authorization: "Bearer " + localStorage.getItem("token")
        }
    });
}

export function update(smtp) {
    return axios.put(`${URL}`, smtp, {
        headers: {
            Authorization: "Bearer " + localStorage.getItem("token")
        }
    });
}

export function deleteSMTP(id) {
    return axios.delete(`${URL}/${id}`, {
        headers: {
            Authorization: "Bearer " + localStorage.getItem("token")
        }
    });
}