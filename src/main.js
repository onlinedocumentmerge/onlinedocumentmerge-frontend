import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';
import CKEditor from '@ckeditor/ckeditor5-vue'
import PerfectScrollbar from 'vue2-perfect-scrollbar'
import 'vue2-perfect-scrollbar/dist/vue2-perfect-scrollbar.css'
import VueIziToast from 'vue-izitoast';
import 'izitoast/dist/css/iziToast.css';
import  "@/plugins/echarts";
 
Vue.use(PerfectScrollbar)
Vue.use(CKEditor);
Vue.use(VueIziToast);
Vue.config.productionTip = false

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')