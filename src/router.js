import Vue from "vue";
import Router from "vue-router";

import User from "./views/user/User.vue";
import EditorPage from "./views/user/EditorPage.vue";
import Profile from "./views/user/Profile.vue";

import Admin from "./views/admin/Admin.vue";
import Account from "./views/admin/Account.vue";
import Statistic from "./views/admin/Statistic.vue";
import Arithmetic from "./views/admin/Arithmetic.vue";
import Test from "./views/admin/Test.vue";

import Login from "./views/Login.vue";
import Register from "./views/Register.vue";
import ForgotPass from './views/ForgotPass.vue'
import Activation from './views/Activation.vue'
import Template from "./views/user/Template.vue";
import Document from "./views/user/Document.vue";
import DataTable from "./views/user/DataTable.vue";
import DataSource from "./views/user/DataSource.vue";
import Job from "./views/user/Job.vue";
import PageNotFound from "./views/404.vue";
import SMTP from './views/user/SMTP.vue'
import Tariff from './views/admin/Tariff.vue'

Vue.use(Router);

const router = new Router({
	mode: "history",
	base: process.env.BASE_URL,
	routes: [
		{
			path: "/login",
			alias: "/",
			name: "login",
			component: Login
		},
		{
			path: "/register",
			name: "register",
			component: Register
		},
		{
			path: "/forgot",
			name: "forgot",
			component: ForgotPass
		},
		{
			path: "/activation",
			name: "activation",
			component: Activation
		},
		{
			path: "/user",
			name: "user",
			component: User,
			children: [
				{
					path: "/template",
					name: "template",
					component: Template
				},
				{
					path: "/editor",
					name: "editor",
					component: EditorPage
				},
				{
					path: "/profile",
					name: "profile",
					component: Profile
				},
				{
					path: "/document",
					name: "document",
					component: Document
				},
				{
					path: "/datatable",
					name: "datatable",
					component: DataTable
				},
				{
					path: "/datasource",
					name: "dataSource",
					component: DataSource
				},
				{
					path: "/job",
					name: "job",
					component: Job
				},
				{
					path: "/smtp",
					name: "smtp",
					component: SMTP
				}
			]
		},
		{
			path: "/admin",
			name: "admin",
			component: Admin,
			children: [
				{
					path: "/account",
					name: "account",
					component: Account
				},
				{
					path: "/statistic",
					name: "statistic",
					component: Statistic
				},
				{
					path: "/arithmetic",
					name: "arithmetic",
					component: Arithmetic
				},
				{
					path: "/tariff",
					name: "tariff",
					component: Tariff
				},
				// {
				// 	path: "/test",
				// 	name: "test",
				// 	component: Test
				// }
			]
		},
		{ path: "/404", component: PageNotFound },
		{ path: "*", redirect: "/404" }
	]
});

// router.beforeEnter(router, (to, from, next) => {
// 	if(location.hostname.split('.').shift() === hostname) {
// 		next()
// 	}
// 	else {
// 		next({ name: 'pageNotFound', params: { '0': to.path } })
// 	}
// })

// router.beforeEach((to, from, next) => {
// 	let web = ["login", "register", "arithmetic"];
// 	let user = [
// 		"template",
// 		"editor",
// 		"profile",
// 		"document",
// 		"datasource",
// 		"spreadsheet",
// 		"job",
// 		"smtp"
// 	];
// 	let admin = ["statistic", "profile", "account"];
// 	let account = JSON.parse(localStorage.getItem("account"));

// 	if (web.includes(to.name)) {
// 		next();
// 	} else {
// 		if (user.includes(to.name) || admin.includes(to.name)) {
// 			if (user.includes(to.name) && account.role.id == 1) {
// 				next();
// 			} else if (admin.includes(to.name) && account.role.id == 2) {
// 				next();
// 			} else {
// 				next('/login')
// 			}
// 		} else {
// 			next();
// 			//next("/404")
// 			//next({ name: 'pageNotFound', params: { '0': to.path } })
// 			//router.push("*");
// 		}
// 	}

// 	// if (web.includes(to.name)) {
// 	// 	next();
// 	// } else {
// 	// 	axios
// 	// 		.post("/api/auth/verify-token", {
// 	// 			token: localStorage.token
// 	// 		})
// 	// 		.then(response => {
// 	// 			if (response.data.verification === true) {
// 	// 				next();
// 	// 			} else {
// 	// 				router.push({
// 	// 					name: "home",
// 	// 					params: {
// 	// 						serverError: true,
// 	// 						serverMsg: "Please login to continue."
// 	// 					}
// 	// 				});
// 	// 			}
// 	// 		})
// 	// 		.catch(response => {
// 	// 			console.log(response);
// 	// 		});
// 	// }
// });

export default router;
